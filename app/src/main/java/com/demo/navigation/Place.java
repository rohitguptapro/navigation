package com.demo.navigation;

public class Place {
    String name;
    String country;
    int img;
    String desc;

    public Place(String name, String country, int img, String desc) {
        this.name = name;
        this.country = country;
        this.img = img;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public int getImg() {
        return img;
    }

    public String getDesc() {
        return desc;
    }
}
