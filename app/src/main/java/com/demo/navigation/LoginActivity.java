package com.demo.navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    String country[] = {"India", "Canada", "Paris"};
    ArrayList<Place> placesList = new ArrayList<>();
    ArrayList<Place> tempList = new ArrayList<>();
    ArrayList<String> tempNames = new ArrayList<>();
    Spinner ctry, place;
    ImageView img;
    TextView desc;
    Button more;
    public static Place obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fillData();
        fillTemp(country[0]);
        ctry = findViewById(R.id.spnCountry);
        place = findViewById(R.id.spnPlaces);
        img = findViewById(R.id.imgPlace);
        desc = findViewById(R.id.txvDesc);
        more = findViewById(R.id.btnShowMore);
        ArrayAdapter aa1 = new ArrayAdapter(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, country);
        ctry.setAdapter(aa1);
        ArrayAdapter aa2 = new ArrayAdapter(this, com.google.android.material.R.layout.support_simple_spinner_dropdown_item, tempNames);
        place.setAdapter(aa2);
        ctry.setOnItemSelectedListener(new SpinnerAction());
        place.setOnItemSelectedListener(new SpinnerAction());
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //move to the main activity
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });
    }

    private class SpinnerAction implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (adapterView.equals(ctry)) {
                fillTemp(country[i]);
                ArrayAdapter aa2 = new ArrayAdapter(LoginActivity.this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, tempNames);
                place.setAdapter(aa2);
            } else if (adapterView.equals(place)) {
                desc.setText(String.valueOf(tempList.get(i).getDesc()));
                img.setImageResource(tempList.get(i).getImg());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public void fillData() {
        placesList.add(new Place("Taj Mahal", country[0], R.drawable.taj_mahal, "The Taj Mahal, is an ivory-white marble mausoleum on the right bank of the river Yamuna in the Indian city of Agra. It was commissioned in 1632 by the Mughal emperor Shah Jahan to house the tomb of his favourite wife, Mumtaz Mahal; it also houses the tomb of Shah Jahan himself. The tomb is the centrepiece of a 17-hectare complex, which includes a mosque and a guest house, and is set in formal gardens bounded on three sides by a crenellated wall. Construction of the mausoleum was essentially completed in 1643, but work continued on other phases of the project for another 10 years. The Taj Mahal complex is believed to have been completed in its entirety in 1653 at a cost estimated at the time to be around ₹32 million, which in 2020 would be approximately ₹70 billion. The construction project employed some 20,000 artisans under the guidance of a board of architects led by the court architect to the emperor, Ustad Ahmad Lahauri. Various types of symbolism have been employed in the Taj to reflect natural beauty and divinity."));
        placesList.add(new Place("The Holy City of Varanasi", country[0], R.drawable.varanasi, "Dating back to the 8th century BC, Varanasi is one of the oldest still inhabited cities in the world. A major pilgrimage center for Hindus, this holy city has long been associated with the mighty Ganges River, one of the faith's most important religious symbols.\n" +
                "\n" +
                "Varanasi offers many reasons to visit, not least of them the chance to explore the Old Quarter adjacent to the Ganges where you'll find the Kashi Vishwanath Temple, built in 1780. The New Vishwanath Temple with its seven separate temples is also of interest.\n" +
                "\n" +
                "Bathing in the Ganges is of great importance to Hindus, and numerous locations known as \"ghats\" feature stairways leading to the water where the faithful bathe before prayers. The largest are Dasashvamedh Ghat and Assi Ghat. The latter, at the confluence of the Ganges and Asi rivers, is considered particularly holy.\n" +
                "\n" +
                "Also worth seeing is Banaras Hindu University, established in 1917 and noted for its massive library with more than a million books, and the superb Bharat Kala Bhavan museum featuring fine collections of miniature paintings, sculptures, palm-leaf manuscripts, and local history exhibits."));
        placesList.add(new Place("The Golden Temple of Amritsar", country[0], R.drawable.golden_temple, "Founded in 1577 by Ram Das, Amritsar is an important hub of Sikh history and culture. The main attraction here is Harmandir Sahib, opened in 1604 and still often referred to as the Golden Temple for its beautiful gold decoration.\n" +
                "\n" +
                "The holiest of India's many Sikh shrines (it also attracts many Hindus and people of other faiths), the temple was built in a blend of Hindu and Islamic styles. Its lower marble section features such flourishes as ornate inlaid floral and animal motifs, while the large golden dome represents a lotus flower, a symbol of purity to Sikhs.\n" +
                "\n" +
                "In addition to its splendid design, visitors are equally impressed with the temple's spiritual atmosphere, an effect enhanced by the prayers continuously chanted from the Sikh holy book and broadcast throughout the complex."));
        placesList.add(new Place("CN Tower", country[1], R.drawable.cn_tower, "The CN Tower is a 553.3 m-high concrete communications and observation tower located in the downtown core of Toronto, Ontario, Canada. Built on the former Railway Lands, it was completed in 1976. Its name \"CN\" originally referred to Canadian National, the railway company that built the tower. Following the railway's decision to divest non-core freight railway assets prior to the company's privatization in 1995, it transferred the tower to the Canada Lands Company, a federal Crown corporation responsible for real estate development. The CN Tower held the record for the world's tallest free-standing structure for 32 years, from 1975 until 2007, when it was surpassed by the Burj Khalifa, and was the world's tallest tower until 2009 when it was surpassed by the Canton Tower. "));
        placesList.add(new Place("Morane Lake", country[1], R.drawable.morane_lake, "Moraine Lake is a glacially fed lake in Banff National Park, 14 kilometres outside the village of Lake Louise, Alberta, Canada. It is situated in the Valley of the Ten Peaks, at an elevation of approximately 1,884 metres. The lake has a surface area of 50 hectares. The lake, being glacially fed, does not reach its crest until middle to late June. When it is full, it reflects a distinctive shade of azure blue. The unique colour is due to the refraction of light off the rock flour deposited in the lake on a continual basis by surrounding glaciers."));
        placesList.add(new Place("Eiffel Tower", country[2], R.drawable.eiffel_tower, "The Eiffel Tower is a wrought-iron lattice tower on the Champ de Mars in Paris, France. It is named after the engineer Gustave Eiffel, whose company designed and built the tower. Locally nicknamed \"La dame de fer\", it was constructed from 1887 to 1889 as the centerpiece of the 1889 World's Fair and was initially criticized by some of France's leading artists and intellectuals for its design, but it has become a global cultural icon of France and one of the most recognizable structures in the world. The Eiffel Tower is the most visited monument with an entrance fee in the world; 6.91 million people ascended it in 2015. The Tower was made a Monument historique in 1964 and named part of UNESCO World Heritage Site in 1991. The tower is 330 metres tall, about the same height as an 81-storey building, and the tallest structure in Paris. Its base is square, measuring 125 metres on each side. During its construction, the Eiffel Tower surpassed the Washington Monument to become the tallest man-made structure in the world, a title it held for 41 years until the Chrysler Building in New York City was finished in 1930."));
        placesList.add(new Place("Louvre Museum", country[2], R.drawable.love_museum, "The Louvre, or the Louvre Museum, is the world's most-visited museum, and a historic landmark in Paris, France. It is the home of some of the best-known works of art, including the Mona Lisa and the Venus de Milo. A central landmark of the city, it is located on the Right Bank of the Seine in the city's 1st arrondissement. At any given point in time, approximately 38,000 objects from prehistory to the 21st century are being exhibited over an area of 72,735 square meters. Attendance in 2021 was 2.8 million due to the COVID-19 pandemic. The museum was closed for 150 days in 2020, and attendance plunged by 72 percent to 2.7 million. Nonetheless, the Louvre still topped the list of most-visited art museums in the world in 2021. The museum is housed in the Louvre Palace, originally built in the late 12th to 13th century under Philip II. Remnants of the Medieval Louvre fortress are visible in the basement of the museum. Due to urban expansion, the fortress eventually lost its defensive function, and in 1546 Francis I converted it into the primary residence of the French Kings. The building was extended many times to form the present Louvre Palace."));
    }

    public void fillTemp(String country) {
        tempList.clear();//remove all items in the temp list
        tempNames.clear();
        for (Place place : placesList)
            if (place.getCountry().equals(country)) {
                tempList.add(place);
                tempNames.add(place.getName());
                obj = place;
            }
    }

}