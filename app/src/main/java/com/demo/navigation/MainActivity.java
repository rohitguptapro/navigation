package com.demo.navigation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView title,desc;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title = findViewById(R.id.txvTitle);
        img = findViewById(R.id.imgView);
        desc = findViewById(R.id.txvDescMore);
        title.setText(LoginActivity.obj.getCountry()+","+LoginActivity.obj.getName());
        img.setImageResource(LoginActivity.obj.getImg());
        desc.setText(LoginActivity.obj.getDesc());
    }

}